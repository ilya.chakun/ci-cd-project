package test.app;

public class TestReq {
    private String name;

    public TestReq(){

    }

    public TestReq(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
